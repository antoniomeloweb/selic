
** Projeto: Visualizador de Taxas Selic **
O aplicativo desenvolvido utiliza as seguintes tecnologias:

1. Spring Boot 
2. Spring MVC
3. BootStrap Data Tables
4. Swagger Api documentation 
5. Vue Js
6. Javascript


---


Ao rodar o aplicativo atrav�s da execu��o da classe:

## br.com.selic.Application.java

Para acessar a aplica��o web, utilize o endere�o:

http://localhost:8080/selic

## API REST

Para acessar a documenta��o da API, acesse o endere�o:

http://localhost:8080/selic/api/swagger-ui.html

METODOS:

```
GET
path /acumuladoAnoAno
lista taxa selic Acumulado Ano a Ano.

GET
path /all
lista completa com todos os dados hist�ricos

GET
path /estimativaAno/{ano}
lista de Estimativas selic dado Ano (obrigatorio) e Mes (opcional)

GET
path /mediaAnoAno
lista Media de taxa selic Ano a Ano (exibe todo o hist�rico)

GET
path /mediaAnual/{ano}
lista taxa selic m�dia Anual para determinado ano
```