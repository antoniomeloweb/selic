	
	var chartAcumuladoObj;
	var ctxChartAcumulado;
	var chartMediaObj;
	var ctxChartMedia;
	
	function generateBackColors(lngt){
		let colors =  ['rgba(255, 99, 132, 0.2)',
				       'rgba(54, 162, 235, 0.2)',
				       'rgba(255, 206, 86, 0.2)',
				       'rgba(75, 192, 192, 0.2)',
				       'rgba(153, 102, 255, 0.2)',
				       'rgba(255, 159, 64, 0.2)'];
		let retorno = [];
		for(let a=0; a < lngt; a++){
			retorno.push(colors[a%colors.length]);
		}
		return retorno;
	}
	function generateBorderColors(len){
		let colors = ['rgba(255,99,132,1)',
			          'rgba(54, 162, 235, 1)',
			          'rgba(255, 206, 86, 1)',
			          'rgba(75, 192, 192, 1)',
			          'rgba(153, 102, 255, 1)',
			          'rgba(255, 159, 64, 1)'];
		let retorno = [];
		for(var a=0; a<len;a++){
			retorno.push(colors[a%colors.length]);
		}
		return retorno;		
	}
	
  function createChart(json, chartId, chartObj, ctxChart, typeChart, labelTxt){
	  
	  let anosDisponiveis = json.map(a => a.data_referencia.split("-")[0]);
	  let valoresSelic = json.map(a => a.estimativa_taxa_selic);
	  let backgroundColorSelic = generateBackColors(valoresSelic.length);
	  let borderColorSelic = generateBorderColors(valoresSelic.length);
	  
	  ctx = document.getElementById(chartId).getContext('2d');
	  
	  chartObj = new Chart(ctx, {
		    type: typeChart,
		    data: {
		      labels: anosDisponiveis,
		      datasets: [{
		        label: labelTxt,
		        data: valoresSelic,
		        backgroundColor: backgroundColorSelic ,
		        borderColor: borderColorSelic,
		        borderWidth: 1
		      }]
		    },
		    options: {
		      scales: {
		        yAxes: [{
		          ticks: {
		            beginAtZero: true
		          }
		        }]
		      }
		    }
		  });				  
  }
	//
	
	
		var app = new Vue({
		    el: '#app',
		    data: {
		        json: null
		    }
		});
		
		var appAcumulado;
		var appMedia;
		
		$(document).ready(function() {


			
			$.getJSON('/selic/acumuladoAnoAno', function (json) {
				appAcumulado = new Vue({
					el: '#appAcumuladoDiv',
					data: {
						chartAcumuladoData: []
					}});			    

				appAcumulado.chartAcumuladoData = json;
			    createChart(json, "chartAcumulado", chartAcumuladoObj, ctxChartAcumulado, "line", "Acumulado anual");
				
			});
			
			$.getJSON('/selic/mediaAnoAno', function (json) {
				appMedia = new Vue({
					el: '#appMediaDiv',
					data: {
						chartMediaData: []
					}});

				appMedia.chartMediaData = json;
			    createChart(json, "chartMedia", chartMediaObj, ctxChartMedia, "bar", "Média anual");
			    
			});
			
            $('#selicTable').DataTable( {
                "ajax": {
                    "url": "/selic/all",
                    "dataSrc": ""
                },
                "columns": [{ "data": "data_referencia", type: 'date' },
                            { "data": "estimativa_taxa_selic"}]
            } );
        } );  
		
