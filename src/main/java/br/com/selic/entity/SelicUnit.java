package br.com.selic.entity;

import java.io.Serializable;
import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

public class SelicUnit implements Serializable{

     private static final long serialVersionUID = 1L;

     private Float estimativa_taxa_selic;
     
     @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
     @JsonSerialize(using = LocalDateSerializer.class)
     @JsonDeserialize(using = LocalDateDeserializer.class)
     private LocalDate data_referencia;
     
     public Float getEstimativa_taxa_selic() {
     
          return estimativa_taxa_selic;
     }
     
     public void setEstimativa_taxa_selic(Float estimativa_taxa_selic) {
     
          this.estimativa_taxa_selic = estimativa_taxa_selic;
     }
     
     public LocalDate getData_referencia() {
     
          return data_referencia;
     }
     
     public void setData_referencia(LocalDate data_referencia) {
     
          this.data_referencia = data_referencia;
     }
}
