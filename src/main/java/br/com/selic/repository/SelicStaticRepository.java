package br.com.selic.repository;

import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.OptionalDouble;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.selic.entity.SelicUnit;

@Repository
public class SelicStaticRepository {
     
     private List<SelicUnit> selic;
     
     @Value("classpath:/static/ESTIMATIVA_SELIC.json")
     private Resource selicDataFile;

     public List<SelicUnit> findAll(){
          if(!Objects.isNull(selic)) {
               return selic;
          } else {
               try {
                    
                    ObjectMapper mapper = new ObjectMapper();
                    selic = Arrays.asList(mapper.readValue(selicDataFile.getInputStream(), SelicUnit[].class));
                    return selic;
                    
                } catch (IOException e) {
                    e.printStackTrace();
                }   
               return null;
          }
     }
     
     public List<SelicUnit> findEstimativa(Integer mes, Integer ano){
          if(Objects.isNull(selic)) {
               findAll();
          }
          
          List<SelicUnit> retorno = selic.stream().filter(slc -> slc.getData_referencia().getYear()==ano).collect(Collectors.toList());
          if(!Objects.isNull(mes)) {
               retorno = retorno.stream().filter(slc -> slc.getData_referencia().getMonthValue()==mes).collect(Collectors.toList());
          }
                
          return retorno;
     }
     
     public Float findMediaAnual(Integer ano) {
          List<SelicUnit> lista = findEstimativa(null, ano);
          
          Double retorno = lista.stream().mapToDouble(SelicUnit::getEstimativa_taxa_selic).average().orElse(Double.valueOf("0"));
          
          return Float.valueOf(retorno.toString());
     }
}
