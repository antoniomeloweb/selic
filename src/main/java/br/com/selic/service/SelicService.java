package br.com.selic.service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.selic.entity.SelicUnit;
import br.com.selic.repository.SelicStaticRepository;

@Service
public class SelicService {
     
     @Autowired
     private SelicStaticRepository selicStaticRepository;
     
     DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
     
     public List<SelicUnit> findAll(){
          return selicStaticRepository.findAll();
     }
     
     public List<SelicUnit> findEstimativa(Integer mes, Integer ano){
          
          return selicStaticRepository.findEstimativa(mes, ano);
     }
     
     public SelicUnit findMediaAnual(Integer ano){
          Float mediaAnual = selicStaticRepository.findMediaAnual(ano);
          String dtStr = ano.toString() + "-12-31";
          SelicUnit retorno = new SelicUnit();
          
          retorno.setEstimativa_taxa_selic(mediaAnual);
          retorno.setData_referencia(LocalDate.parse(dtStr, formatter));
          
          return retorno;
     }
     
     public SelicUnit findAcumuladoAnual(Integer ano){
          List<SelicUnit> lista = selicStaticRepository.findEstimativa(null, ano);
          
          Double acumulado = lista.stream().mapToDouble(s -> s.getEstimativa_taxa_selic()).sum();
          
          String dtStr = ano.toString() + "-12-31";
          SelicUnit retorno = new SelicUnit();
          
          retorno.setEstimativa_taxa_selic(acumulado.floatValue());
          retorno.setData_referencia(LocalDate.parse(dtStr, formatter));
          
          return retorno;
     }
     
     public List<SelicUnit> findAcumuladoAnoAno(){
          List<Integer> anosDisponiveis = findAll().stream().map(slc -> slc.getData_referencia().getYear()).distinct().collect(Collectors.toList());
          
          List<SelicUnit> retorno = new ArrayList<SelicUnit>();
          
          for(Integer ano: anosDisponiveis) {
               retorno.add(findAcumuladoAnual(ano));
          }
          
          return retorno;
     }
     
     public List<SelicUnit> findMediaAnoAno(){
          List<Integer> anosDisponiveis = findAll().stream().map(slc -> slc.getData_referencia().getYear()).distinct().collect(Collectors.toList());
          
          List<SelicUnit> retorno = new ArrayList<SelicUnit>();

          for(Integer ano: anosDisponiveis) {
               retorno.add(findMediaAnual(ano));
          }
          
          return retorno;
     }
}
