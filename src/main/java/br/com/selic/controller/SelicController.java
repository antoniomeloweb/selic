package br.com.selic.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.selic.entity.SelicUnit;
import br.com.selic.service.SelicService;
import io.swagger.annotations.ApiOperation;

@Controller
@RequestMapping("/")
public class SelicController {
       
     @Autowired
     private SelicService selicService;
     
     @ApiOperation(value="selicHtml", hidden=true)
     @RequestMapping(value = {"/","/HTML"}, method = RequestMethod.GET)
     public String listarAll(Model model) {
           /*List<SelicUnit> listaSelic = new ArrayList<SelicUnit>();
           listaSelic = selicService.findAll();
           
           model.addAttribute("listaSelic", listaSelic);*/
           return "index";
     }
     
     @RequestMapping(value = "/all", method = RequestMethod.GET)
     public ResponseEntity<List<SelicUnit>> listaAllRest() {

          return new ResponseEntity<List<SelicUnit>>(selicService.findAll(), HttpStatus.OK);
     }     
     
     @RequestMapping(value = "/estimativaAno/{ano}", method = RequestMethod.GET)
     public ResponseEntity<List<SelicUnit>> listaEstimativaAnoMes(@PathVariable(value="ano") Integer ano,
                                                    @RequestParam(required=false, name="mes") Integer mes) {
          
          return new ResponseEntity<List<SelicUnit>>(selicService.findEstimativa(mes, ano), HttpStatus.OK);
     }
     
     @RequestMapping(value = "/mediaAnual/{ano}", method = RequestMethod.GET)
     public ResponseEntity<SelicUnit> listaEstimativaAnoMes(@PathVariable(value="ano") Integer ano) {
          
          SelicUnit retorno = selicService.findMediaAnual(ano);
          
          return new ResponseEntity<SelicUnit>(retorno, HttpStatus.OK);
     }
     
     @RequestMapping(value = "/mediaAnoAno", method = RequestMethod.GET)
     public ResponseEntity<List<SelicUnit>> listaMediaAnoAno() {
          
          List<SelicUnit> retorno = selicService.findMediaAnoAno();
          
          return new ResponseEntity<List<SelicUnit>>(retorno, HttpStatus.OK);
     }
     
     @RequestMapping(value = "/acumuladoAnoAno", method = RequestMethod.GET)
     public ResponseEntity<List<SelicUnit>> listaAcumuladoAnoAno() {
          
          List<SelicUnit> retorno = selicService.findAcumuladoAnoAno();
          
          return new ResponseEntity<List<SelicUnit>>(retorno, HttpStatus.OK);
     }
}